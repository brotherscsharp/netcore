﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ParrotWings.Core.Model
{
    [Table("Student")]
    public class Student
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
    }
}
