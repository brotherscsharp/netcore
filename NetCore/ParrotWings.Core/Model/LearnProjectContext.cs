﻿using System;
using Microsoft.EntityFrameworkCore;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Core.Model
{
    public class LearnProjectContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        //public IConfiguration Configuration { get; }

        // Parrot Wings tables
        public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionHistory> TransactionHistory { get; set; }

        public LearnProjectContext(DbContextOptions<LearnProjectContext> options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }
        /*
        public SchoolContext(DbContextOptions options, IConfiguration configuration) : base(options)
        {
            Configuration = configuration;

            this.Database.EnsureCreated();
            this.Database.Migrate(); //i have tried this too 
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("Connection"));
        }*/
    }
}
