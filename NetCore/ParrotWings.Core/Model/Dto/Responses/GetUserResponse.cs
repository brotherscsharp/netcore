﻿using ParrotWings.Core.Interfaces;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Core.Model.Dto.Responses
{
    public class GetUserResponse : IResponse
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
        public UserMeta User { get; set; }
    }
}
