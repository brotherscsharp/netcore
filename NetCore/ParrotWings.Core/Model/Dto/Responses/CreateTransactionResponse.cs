﻿using ParrotWings.Core.Interfaces;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Core.Model.Dto.Responses
{
    public class CreateTransactionResponse : IResponse
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
        public Transaction Transaction { get; set; }
    }
}
