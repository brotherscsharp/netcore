﻿using System;
namespace ParrotWings.Core.Model.Dto.Responses
{
    public class TransactionHistoryResponse
    {
        public long Id { get; set; }
        public long TransactionId { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public string Receiver { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
