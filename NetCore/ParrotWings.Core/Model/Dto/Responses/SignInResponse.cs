﻿using ParrotWings.Core.Interfaces;

namespace ParrotWings.Core.Model.Dto.Responses
{
    public class SignInResponse : IResponse
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
        public string AccessToken { get; set; }
        public long UserId { get; set; }
    }
}
