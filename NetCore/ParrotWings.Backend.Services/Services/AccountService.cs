﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ParrotWings.Backend.Services.Extensions;
using ParrotWings.Backend.Services.Helpers;
using ParrotWings.Core;
using ParrotWings.Core.Model;
using ParrotWings.Core.Model.Dto.Requests;
using ParrotWings.Core.Model.Dto.Responses;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Backend.Services.Services
{
    public class AccountService
    {
        private readonly TokenService _tokenService;
        private readonly LearnProjectContext _context;
        private readonly ILogger<AccountService> _logService;

        public AccountService(TokenService tokenService, LearnProjectContext context, ILogger<AccountService> logService)
        {
            _tokenService = tokenService;
            _context = context;
            _logService = logService;
        }

        public async Task<SignUpResponse> SignUp(User user)
        {
            if (UserExists(user.Email))
            {
                _logService.LogWarning($"User with such email is currently exists: {user.Email}");
                return new SignUpResponse()
                {
                    IsSuccess = false,
                    Error = "User with such email is currently exists"
                };
            }

            if (user.Password != user.RepeatePassword)
            {
                _logService.LogWarning($"Passwords are not mutch");
                return new SignUpResponse()
                {
                    IsSuccess = false,
                    Error = "Passwords are not mutch"
                };
            }

            user.Password = PasswordHelper.Encrypt(user.Password);
            user.Balance = Config.DefaultBalance;
            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return new SignUpResponse()
            {
                IsSuccess = true,
                AccessToken = _tokenService.GenerateToken(user),
                UserId = user.Id
            };
        }

        public async Task<SignInResponse> SignIn(SignInRequest request)
        {
            var user = await _context.Users
                .Where(one => one.Email == request.Email)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                return new SignInResponse()
                {
                    IsSuccess = false,
                    Error = "User with such email not found"
                };
            }

            if (!PasswordHelper.Check(request.Password, user.Password))
            {
                return new SignInResponse()
                {
                    IsSuccess = false,
                    Error = "Incorrect password"
                };
            }

            return new SignInResponse()
            {
                IsSuccess = true,
                AccessToken = _tokenService.GenerateToken(user),
                UserId = user.Id
            };
        }

        internal void UpdateUser(User userSender)
        {
            _context.Users.Update(userSender);
        }

        public async Task<GetUsersResponse> GetUsers(string filter)
        {
            try
            {

                var users = (filter == null) ? await _context.Users.ToListAsync()
                                        : await _context.Users.Where(user => user.Name.ToLower().StartsWith(filter) || user.Email.ToLower().StartsWith(filter))
                                                              .ToListAsync();
                return new GetUsersResponse()
                {
                    IsSuccess = true,
                    Users = users.ToCollection()
                };
            }
            catch (Exception ex)
            {
                _logService.LogError($"Getting user error: filter = {filter} {ex.Message} {ex.StackTrace}");
                return new GetUsersResponse()
                {
                    IsSuccess = false,
                    Users = null
                };
            }
        }

        internal async Task<User> GetUserData(long id)
        {
            try
            {
                var user = await _context.Users.FindAsync(id);
                return user;
            }
            catch (Exception ex)
            {
                _logService.LogError($"Getting user error: useriId = {id} {ex.Message} {ex.StackTrace}");
                return null;
            }
        }

        public async Task<UserMeta> GetUser(long id)
        {
            var user = await GetUserData(id);
            return user?.ToModel();
        }

        private bool UserExists(string email)
        {
            return _context.Users.Any(e => e.Email == email);
        }
    }

}
