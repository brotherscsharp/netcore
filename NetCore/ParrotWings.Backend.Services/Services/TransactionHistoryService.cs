﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ParrotWings.Core.Model;
using ParrotWings.Core.Model.Dto.Responses;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Backend.Services.Services
{
    public class TransactionHistoryService
    {
        private readonly LearnProjectContext _context;
        private readonly ILogger<TransactionHistoryService> _logService;

        public TransactionHistoryService(LearnProjectContext context, ILogger<TransactionHistoryService> logService)
        {
            _context = context;
            _logService = logService;
        }


        internal async Task<GetUserTransactionsResponse> GetUserHistory(long userId)
        {
            try
            {
                // var history = await _context.TransactionHistory.Where(h => h.UserId == userId).ToListAsync();

                var query = from t in _context.TransactionHistory.Where(h => h.UserId == userId)
                          from u in _context.Users.Where(a => a.Id == t.ReceiverId).DefaultIfEmpty()
                          select new TransactionHistoryResponse
                          {
                              Id = t.Id,
                              TransactionId = t.TransactionId,
                              Amount = t.Amount,
                              Balance = t.Balance,
                              CreatedAt = t.CreatedAt,
                              Receiver = u.Name
                          };
                var history = await query.OrderByDescending(h => h.TransactionId).ToListAsync();

                return new GetUserTransactionsResponse
                {
                    IsSuccess = true,
                    TransactionHistory = history
                };

            }
            catch (Exception ex)
            {
                _logService.LogError($"Getting user error: userId = {userId} {ex.Message} {ex.StackTrace}");
                return new GetUserTransactionsResponse()
                {
                    IsSuccess = false,
                    Error = "Could not get user's transactions"
                };
            }
        }

        internal async Task<TransactionHistory> CreateHistory(long transactionId,
                                                                            long userSenderId,
                                                                            long userReceiverId,
                                                                            decimal balance,
                                                                            decimal amount,
                                                                            bool isSenderHistory = false
                                                                            )
        {
            var history = new TransactionHistory
            {
                TransactionId = transactionId,
                Amount = amount,
                Balance = balance,
                SenderId = userSenderId,
                ReceiverId = userReceiverId,
                UserId = isSenderHistory ? userSenderId : userReceiverId,
                CreatedAt = DateTime.UtcNow
            };
            _context.TransactionHistory.Add(history);
            await _context.SaveChangesAsync();
            return history;
        }
    }
}
