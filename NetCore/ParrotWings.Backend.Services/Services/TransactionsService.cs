﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ParrotWings.Core.Model;
using ParrotWings.Core.Model.Dto.Responses;
using ParrotWings.Core.Model.Dto.Value;

namespace ParrotWings.Backend.Services.Services
{
    public class TransactionsService
    {
        private readonly LearnProjectContext _context;
        private readonly AccountService _accountService;
        private readonly TransactionHistoryService _historyService;
        private readonly ILogger<TransactionsService> _logService;

        public TransactionsService(LearnProjectContext context, AccountService accountService, TransactionHistoryService historyService, ILogger<TransactionsService> logService)
        {
            _context = context;
            _accountService = accountService;
            _historyService = historyService;
            _logService = logService;
        }

        public async Task<GetUserTransactionsResponse> GetUsersTransactions(long userId)
        {
            return await _historyService.GetUserHistory(userId);
        }

        public async Task<CreateTransactionResponse> CreateTransaction(long userId, decimal amount, long userSenderId)
        {
            var userSender = await _accountService.GetUserData(userSenderId);
            var userReceiver = await _accountService.GetUserData(userId);

            if (userSender == null) return new CreateTransactionResponse
            {
                IsSuccess = false,
                Error = "User is not authorised"
            };

            if (userReceiver == null) return new CreateTransactionResponse
            {
                IsSuccess = false,
                Error = "User recipient does not exists"
            };

            if (userReceiver.Balance < amount) return new CreateTransactionResponse
            {
                IsSuccess = false,
                Error = "User does not have enough funds"
            };

            var entity = new Transaction
            {
                Amount = amount,
                ReceiverId = userReceiver.Id,
                SenderId = userSender.Id
            };

            userSender.Balance -= amount;
            userReceiver.Balance += amount;

            //using var transaction = await _context.Database.BeginTransactionAsync();

            using (var transaction = await _context.Database.BeginTransactionAsync()) // inMemory DB does not support transactions
            {
                try
                {
                    var newTransaction = _context.Transactions.Add(entity);

                    // history sender
                    await _historyService.CreateHistory(newTransaction.Entity.Id, userSender.Id, userReceiver.Id, userSender.Balance, amount, true);

                    // history receiver
                    await _historyService.CreateHistory(newTransaction.Entity.Id, userSender.Id, userReceiver.Id, userReceiver.Balance, amount);

                    // update sender
                    _accountService.UpdateUser(userSender);

                    // update receiver
                    _accountService.UpdateUser(userReceiver);

                    _context.SaveChanges();

                    await transaction.CommitAsync();

                    return new CreateTransactionResponse
                    {
                        IsSuccess = true,
                        Transaction = newTransaction.Entity
                    };
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logService.LogError($"Create transaction error: {ex.Message} {ex.StackTrace}");
                    return new CreateTransactionResponse()
                    {
                        IsSuccess = false,
                        Error = "Create transaction error"
                    };
                }
            }
        }
    }
}
