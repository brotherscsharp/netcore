﻿using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using ParrotWings.Backend.Services.Helpers;
using ParrotWings.Core;
using ParrotWings.Core.Model;

namespace ParrotWings.Backend.Services.Services
{
    public class TokenService
    {
        public string GenerateToken(User user)
        {
            var credentials = new SigningCredentials(SecurityKeyHelper.GetSecuirtyKey(), SecurityAlgorithms.RsaSha256);

            //  Finally create a Token
            var header = new JwtHeader(credentials);

            var nbf = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var exp = nbf + 3600 * 1000;

            //Some PayLoad that contain information about the  customer
            var payload = new JwtPayload
            {
                { "nbf", nbf },
                { "exp", exp },
                { "iss", Config.ValidIssuer },
                { "aud", Config.ValidAudience },
                { "id", user.Id }
            };

            //
            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();

            // Token to String so you can use it in your client
            var tokenString = handler.WriteToken(secToken);

            return tokenString;
        }
    }
}
