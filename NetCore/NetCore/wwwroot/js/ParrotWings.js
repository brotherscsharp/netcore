﻿const recordsPerPage = 10;

const auth = document.getElementById("auth-content");
const mainContent = document.getElementById("main-content");
const currentUserName = document.getElementById("currentUserName");
const currentUserBalance = document.getElementById("currentUserBalance");
const logoutButton = document.getElementById("logoutButton");

const signInBlock = document.getElementById("sign-in");
const signUpBlock = document.getElementById("sign-up");
const loginBtn = document.getElementById('loginBtn');
const registerBtn = document.getElementById('registerBtn');
const goToRegistration = document.getElementById("goToRegistration");
const goToSignIn = document.getElementById("goToSignIn");

const sendFundsTab = document.getElementById("nav-profile-tab");
const sendFundsBtn = document.getElementById("sendFundsBtn");

let _cookie;

const setCookie = (name, value, options = {}) => {
    options = {
        path: '/',
        ...options
    };

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

const getCookie = (name) => {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

const deleteCookie = (name) => {
    setCookie(name, "", {
        'max-age': -1
    })
}

const setMainContentVisibility = (isVisible) => {
    if (isVisible) {
        mainContent.classList.toggle('hidden', false);
        auth.classList.toggle('hidden', true);
    } else {
        mainContent.classList.toggle('hidden', true);
        auth.classList.toggle('hidden', false);
    }
}

const showTeast = (message, type = 'success') => {
    var alert = document.getElementById('systemMessages')

    switch (type) {
        case 'success':
            alert.classList.remove('alert-danger');
            alert.classList.remove('alert-warning');
            alert.classList.add('alert-success');
            break;
        case 'warning':
            alert.classList.remove('alert-danger');
            alert.classList.remove('alert-success');
            alert.classList.add('alert-warning');
            break;
        case 'error':
            alert.classList.remove('alert-success');
            alert.classList.remove('alert-warning');
            alert.classList.add('alert-danger');
            break;
    }
    alert.innerText = message;
    alert.classList.toggle('fade')
    setTimeout(() => alert.classList.toggle('fade'), 2000);
};

const getCurrentUserInfo = () => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + _cookie);

    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    let sss = getCookie('userId');
    fetch("/api/Users/" + getCookie('userId'), requestOptions)
        .then(response => response.json())
        .then((result) => {
            if (result) {
                currentUserBalance.innerText = `${result.balance} PW`;
                currentUserName.innerText = result.name;
            }
        })
        .catch(error => {
            showTeast('Unable to load sign in.', 'error');
            console.error('Unable to load sign in.', error);
        });
}

const addTableCell = (row, index, value) => {
    var newCell = row.insertCell(index);
    var newText = document.createTextNode(value);
    newCell.appendChild(newText);
    return newCell;
}

const numPages = (items) => {
    return Math.ceil(items / recordsPerPage);
}

function getCurrentUserTransactions () {
    return new Promise((resolve, reject) => {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + _cookie);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("/api/Transactions?userId=" + getCookie('userId'), requestOptions)
            .then(response => response.json())
            .then((result) => {
                if (result.isSuccess) {
                    resolve(result.transactionHistory);
                }
            })
            .catch(error => {
                showTeast('Unable to load transactions', 'error');
                console.error('Unable to load transactions', error);
                reject([]);
            });
    });
}

const generatePaginator = (pages) => {
    let pagination = document.getElementById('pagination')
    let innerHtml = '';
    for (let i = 0; i < pages; i++) {
        innerHtml += `<li class="page-item"><a class="page-link" onClick="loadTransactionsPagination(${i + 1})">${i + 1}</a></li>`;
    }
    pagination.innerHTML = innerHtml;
}

const loadTransactionsPagination = (page = 1) => {

    getCurrentUserTransactions().then(transactions => {
        let pages = numPages(transactions.length);
        generatePaginator(pages);

        let table = document.getElementById('transactionsTable').getElementsByTagName('tbody')[0];
        let rowIndex = 0;
        table.innerHTML = '';
        if (transactions && transactions.length > 0) {
            for (let i = (page - 1) * recordsPerPage; i < (page * recordsPerPage); i++) {

                const item = transactions[i];
                if (item) {
                    var newRow = table.insertRow(rowIndex);

                    addTableCell(newRow, 0, i + 1);
                    addTableCell(newRow, 1, item.receiver);
                    addTableCell(newRow, 2, `${item.amount} PW`);
                    addTableCell(newRow, 3, `${item.balance} PW`);
                    addTableCell(newRow, 4, new Date(item.createdAt).toLocaleString());
                    rowIndex++;
                }
            }
        }
    });
};

const removeOptions = (selectElement) => {
    var i, L = selectElement.options.length - 1;
    for (i = L; i >= 0; i--) {
        selectElement.remove(i);
    }
}

const getUsers = () => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + _cookie);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch("/api/Users", requestOptions)
        .then(response => response.json())
        .then((result) => {
            if (result.isSuccess) {
                let select = document.getElementById('selectUserName');
                removeOptions(select);

                let userId = getCookie('userId');
                let users = result.users.filter(u => u.id != userId);
                if (users.length > 0) {
                    for (let i = 0; i < users.length; i++) {
                        const item = users[i];

                        let option = document.createElement("option");
                        option.value = item.id;
                        option.text = item.name;
                        select.add(option);
                    }
                }
            }
        })
        .catch(error => {
            showTeast('Unable to load users', 'error');
            console.error('Unable to load users', error);
        });
}

const checkToken = () => {
    if (getCookie('token') && getCookie('token') !== 'undefined') {
        // token exists
        _cookie = getCookie('token');
        setMainContentVisibility(true);

        getCurrentUserInfo();
        loadTransactionsPagination();
    } else {
        // token does not exist
        setMainContentVisibility(false);
    }
}

const loginValidation = (email, password) => {
    if (email.trim() == '') {
        showTeast('Email cannot be empty', 'warning');
        return false;
    }

    if (password.trim() == '') {
        showTeast('Password cannot be empty', 'warning');
        return false;
    }
    return true;
}

const login = (email, password) => {

    if (!loginValidation(email, password)) return;

    const item = {
        email: email.trim(),
        password: password.trim()
    };

    fetch('/auth/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(item)
    })
        .then(response => response.json())
        .then((result) => {
            if (result.isSuccess) {
                setCookie('token', result.accessToken);
                setCookie('userId', result.userId);
                
                checkToken();
            }
        })
        .catch(error => {
            showTeast('Unable to sign in.', 'error');
            console.error('Unable to sign in.', error);
        });
}

const registrationValidation = (userName, email, password, rePassword) => {
    if (userName.trim() == '') {
        showTeast('User Name cannot be empty', 'warning');
        return false;
    }

    if (email.trim() == '') {
        showTeast('Email cannot be empty', 'warning');
        return false;
    }

    if (password.trim() == '') {
        showTeast('Password cannot be empty', 'warning');
        return false;
    }

    if (rePassword.trim() == '') {
        showTeast('RePassword cannot be empty', 'warning');
        return false;
    }
    return true;
}

const register = (userName, email, password, rePassword) => {

    if (!registrationValidation(userName, email, password, rePassword)) return;

    const item = {
        name: userName,
        email: email,
        password: password,
        repeatepassword: rePassword
    };

    fetch('/auth/register', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(item)
    })
        .then(response => response.json())
        .then((result) => {
            if (result.isSuccess) {
                setCookie('token', result.accessToken);
                setCookie('userId', result.userId);

                checkToken();
            }
        })
        .catch(error => {
            showTeast('Unable to sign up.', 'error');
            console.error('Unable to sign up.', error);
        });
}

loginBtn.addEventListener('click', async () => {
    const userName = document.getElementById("inputUserName").value;
    const password = document.getElementById("inputPassword").value;
    login(userName, password);
});

registerBtn.addEventListener('click', async () => {
    const userName = document.getElementById("inputRegistrationUserName").value;
    const email = document.getElementById("inputEmail").value;
    const password = document.getElementById("inputRegistrationPassword").value;
    const rePassword = document.getElementById("inputRePassword").value;
    register(userName, email, password, rePassword);
});

logoutButton.addEventListener('click', async () => {
    deleteCookie('token');
    deleteCookie('userId');
    checkToken();
});

const setSignInBlockVisibility = (isVisible) => {
    if (isVisible) {
        signInBlock.classList.toggle('hidden', false);
        signUpBlock.classList.toggle('hidden', true);
    } else {
        signInBlock.classList.toggle('hidden', true);
        signUpBlock.classList.toggle('hidden', false);
    }
}

goToSignIn.addEventListener('click', async () => {
    setSignInBlockVisibility(true);
});

goToRegistration.addEventListener('click', async () => {
    setSignInBlockVisibility(false);

});

sendFundsTab.addEventListener('click', async () => {
    getUsers();
});

const sendFunds = (userId, amount) => {
    const item = {
        senderUserId: getCookie('userId'),
        userId: userId,
        amount: amount,
    };

    fetch('/api/Transactions', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(item)
    })
        .then(response => response.json())
        .then((result) => {
            if (result.isSuccess) {
                getCurrentUserInfo();
                getCurrentUserTransactions();

                document.getElementById("inputAmount").value = "";
                showTeast('Funds are sucessfully sent');
            }
        })
        .catch(error => {
            showTeast('Unable to send funds.', 'error');
            console.error('Unable to send funds.', error);
        });
}

sendFundsBtn.addEventListener('click', async () => {
    const userId = document.getElementById("selectUserName").value;
    const amount = document.getElementById("inputAmount").value;

    sendFunds(userId, amount);
});


checkToken();