﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
const divInstall = document.getElementById('installContainer');
const butInstall = document.getElementById('butInstall');
const bageIncrement = document.getElementById('bageIncrement');
const bageClear = document.getElementById('bageClear');


/* Put code here */



/* Only register a service worker if it's supported */
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/service-worker.js');
}

/**
 * Warn the page must be served over HTTPS
 * The `beforeinstallprompt` event won't fire if the page is served over HTTP.
 * Installability requires a service worker with a fetch event handler, and
 * if the page isn't served over HTTPS, the service worker won't load.
 */
if (window.location.protocol === 'http:') {
    const requireHTTPS = document.getElementById('requireHTTPS');
    const link = requireHTTPS.querySelector('a');
    link.href = window.location.href.replace('http://', 'https://');
    requireHTTPS.classList.remove('hidden');
}

window.addEventListener('beforeinstallprompt', (event) => {
    console.log('👍', 'beforeinstallprompt', event);
    // Stash the event so it can be triggered later.
    window.deferredPrompt = event;
    // Remove the 'hidden' class from the install button container
    divInstall.classList.toggle('hidden', false);
});

butInstall.addEventListener('click', async () => {
    console.log('👍', 'butInstall-clicked');
    const promptEvent = window.deferredPrompt;
    if (!promptEvent) {
        // The deferred prompt isn't available.
        return;
    }
    // Show the install prompt.
    promptEvent.prompt();
    // Log the result
    const result = await promptEvent.userChoice;
    console.log('👍', 'userChoice', result);
    // Reset the deferred prompt variable, since
    // prompt() can only be called once.
    window.deferredPrompt = null;
    // Hide the install button.
    divInstall.classList.toggle('hidden', true);
});

window.addEventListener('appinstalled', (event) => {
    console.log('👍', 'appinstalled', event);
    // Clear the deferredPrompt so it can be garbage collected
    window.deferredPrompt = null;
});

let bageNumbers = 0;
bageIncrement.addEventListener('click', async () => {
    console.log('👍', 'bageIncrement-clicked');
    bageNumbers++;
    setBadge(bageNumbers);
    //fetch('manifest.json');
});

bageClear.addEventListener('click', async () => {
    console.log('👍', 'bageClear-clicked');
    clearBadge();
    bageNumbers = 0;
});

function setBadge(...args) {
    if (navigator.setAppBadge) {
        navigator.setAppBadge(...args);
    } else if (navigator.setExperimentalAppBadge) {
        navigator.setExperimentalAppBadge(...args);
    } else if (window.ExperimentalBadge) {
        window.ExperimentalBadge.set(...args);
    }
}

function clearBadge() {
    if (navigator.clearAppBadge) {
        navigator.clearAppBadge();
    } else if (navigator.clearExperimentalAppBadge) {
        navigator.clearExperimentalAppBadge();
    } else if (window.ExperimentalBadge) {
        window.ExperimentalBadge.clear();
    }
}