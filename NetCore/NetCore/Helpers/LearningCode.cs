﻿using System;
using System.Security.Cryptography;

namespace NetCore.Helpers
{
    public class LearningCode
    {
        public static byte[] GenerateRandomNumber(int length)
        {
            using var randomNumberGenerator = new RNGCryptoServiceProvider();

            var randomNumber = new byte[length];
            randomNumberGenerator.GetBytes(randomNumber);
            return randomNumber;
        }
    }
}
