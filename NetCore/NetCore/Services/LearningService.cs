﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NetCore.Models;
using NetCore.Models.Dto.Responses;

namespace NetCore.Helpers
{
    public class LearningService
    {
        public LearningService()
        {
        }

        #region Random numbers start
        internal byte[] GenerateRandomNumber(int length)
        {
            using var randomNumberGenerator = new RNGCryptoServiceProvider();

            var randomNumber = new byte[length];
            randomNumberGenerator.GetBytes(randomNumber);
            return randomNumber;
        }

        internal string GenerateRandomNumbersAsString(int length, int items)
        {
            var str = new StringBuilder();
            for (int i = 0; i < items; i++)
            {
                var randomNumber = GenerateRandomNumber(length);
                str.AppendLine(Convert.ToBase64String(randomNumber));
            }
            return str.ToString();
        }

        internal List<byte[]> GenerateRandomNumberAsList(int length, int items)
        {
            var str = new List<byte[]>();
            for (int i = 0; i < items; i++)
            {
                var randomNumber = GenerateRandomNumber(length); ;
                str.Add(randomNumber);
            }
            return str;
        }
        #endregion


        #region Hash start

        internal object Hashing()
        {
            const string originalMessage = "Original Message to hash";
            const string originalMessage2 = "This is another Message to hash";
            /*
            var md5 = ComputeHashShaMd5(Encoding.UTF8.GetBytes(originalMessage));
            var md52 = ComputeHashShaMd5(Encoding.UTF8.GetBytes(originalMessage2));

            var sha1 = ComputeHashSha1(Encoding.UTF8.GetBytes(originalMessage));
            var sha12 = ComputeHashSha1(Encoding.UTF8.GetBytes(originalMessage2));

            var sha256 = ComputeHashSha256(Encoding.UTF8.GetBytes(originalMessage));
            var sha2562 = ComputeHashSha256(Encoding.UTF8.GetBytes(originalMessage2));

            var sha512 = ComputeHashSha512(Encoding.UTF8.GetBytes(originalMessage));
            var sha5122 = ComputeHashSha512(Encoding.UTF8.GetBytes(originalMessage2));
            */
            /*
            // HMAC
            var key = GenerateRandomNumber(32);
            var md5 = ComputeHmacMD5(Encoding.UTF8.GetBytes(originalMessage), key);
            var md52 = ComputeHmacMD5(Encoding.UTF8.GetBytes(originalMessage2), key);

            var sha1 = ComputeHmacSha1(Encoding.UTF8.GetBytes(originalMessage), key);
            var sha12 = ComputeHmacSha1(Encoding.UTF8.GetBytes(originalMessage2), key);

            var sha256 = ComputeHmacSha256(Encoding.UTF8.GetBytes(originalMessage), key);
            var sha2562 = ComputeHmacSha256(Encoding.UTF8.GetBytes(originalMessage2), key);

            var sha512 = ComputeHmacSha512(Encoding.UTF8.GetBytes(originalMessage), key);
            var sha5122 = ComputeHmacSha512(Encoding.UTF8.GetBytes(originalMessage2), key);
            
            return new
            {
                md5 = md5,
                md5_2 = md52,
                sha1 = sha1,
                sha1_2 = sha12,
                sha256 = sha256,
                sha256_2 = sha2562,
                sha512 = sha512,
                sha512_2 = sha5122,
            };
            */


            // hash password with salt
            var password = "hyfh4Ez9hNLRu7";
            var key = GenerateRandomNumber(32); // generate salt
            /*
            var hashedPassword = HashPasswordWithSalt(Encoding.UTF8.GetBytes(password), key);
            return new
            {
                password = password,
                salt = key,
                hashedPassword = Convert.ToBase64String(hashedPassword)
            };
            */

            //Hash password. Password Based key derivation function
            var hash100 = HashPasswordTest(password, 100);
            var hash1000 = HashPasswordTest(password, 1000);
            var hash10000 = HashPasswordTest(password, 10000);
            var hash50000 = HashPasswordTest(password, 50000);
            var hash100000 = HashPasswordTest(password, 100000);
            var hash200000 = HashPasswordTest(password, 200000);
            var hash500000 = HashPasswordTest(password, 500000);

            return new
            {
                password = password,
                salt = key,
                hash100 = hash100,
                hash1000 = hash1000,
                hash10000 = hash10000,
                hash50000 = hash50000,
                hash100000 = hash100000,
                hash200000 = hash200000,
                hash500000 = hash500000,
            };

        }

        #region HMAC

        private const int KeySize = 32;


        internal byte[] ComputeHmacSha1(byte[] toBeHashed, byte[] key)
        {
            using var hmac = new HMACSHA1(key);
            return hmac.ComputeHash(toBeHashed);
        }

        internal byte[] ComputeHmacSha256(byte[] toBeHashed, byte[] key)
        {
            using var hmac = new HMACSHA256(key);
            return hmac.ComputeHash(toBeHashed);
        }

        internal byte[] ComputeHmacSha512(byte[] toBeHashed, byte[] key)
        {
            using var hmac = new HMACSHA512(key);
            return hmac.ComputeHash(toBeHashed);
        }

        internal byte[] ComputeHmacMD5(byte[] toBeHashed, byte[] key)
        {
            using var hmac = new HMACMD5(key);
            return hmac.ComputeHash(toBeHashed);
        }

        #endregion


        #region Hashing pasword


        internal byte[] Combine(byte[] first, byte[] last)
        {
            var ret = new byte[first.Length + last.Length];

            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(last, 0, ret, first.Length, last.Length);

            return ret;
        }

        internal byte[] HashPasswordWithSalt(byte[] toBeHashed, byte[] salt)
        {
            using var sha256 = SHA256.Create();
            return sha256.ComputeHash(Combine(toBeHashed, salt));
        }

        // password based key derivation functions
        internal byte[] HashPassword(byte[] password, byte[] salt, int rounds)
        {
            using(var rfc2898 = new Rfc2898DeriveBytes(password, salt,rounds))
            {
                return rfc2898.GetBytes(32);
            }
        }

        internal string HashPasswordTest(string password, int rounds)
        {
            var sw = new Stopwatch();
            sw.Start();

            var hashedPassword = HashPassword(Encoding.UTF8.GetBytes(password), GenerateRandomNumber(32), rounds);

            sw.Stop();
            return $"Password: {Convert.ToBase64String(hashedPassword)}, rounds: {rounds}, time: {sw.ElapsedMilliseconds}";
        }
        #endregion

        internal byte[] ComputeHashSha1(byte[] toBeHashed)
        {
            using var sha1 = SHA1.Create();
            return sha1.ComputeHash(toBeHashed);
        }

        internal byte[] ComputeHashSha256(byte[] toBeHashed)
        {
            using var sha256 = SHA256.Create();
            return sha256.ComputeHash(toBeHashed);
        }

        internal byte[] ComputeHashSha512(byte[] toBeHashed)
        {
            using var sha512 = SHA512.Create();
            return sha512.ComputeHash(toBeHashed);
        }

        internal byte[] ComputeHashShaMd5(byte[] toBeHashed)
        {
            using var md5 = MD5.Create();
            return md5.ComputeHash(toBeHashed);
        }
        #endregion


        #region Symmetric Encrypion

        internal object SymmetricEncryption()
        {
           

            const string original = "Text to encrypt";

            // DES
            //var key = GenerateRandomNumber(8);
            //var iv = GenerateRandomNumber(8);
            //var encrypted = Encrypt(Encoding.UTF8.GetBytes(original), key, iv);
            // var decrypted = Decrypt(encrypted, key, iv);

            // Triple DES
            /*var key = GenerateRandomNumber(24);
            //var key = GenerateRandomNumber(16);
            var iv = GenerateRandomNumber(8);
            var encrypted = EncryptTripleDes(Encoding.UTF8.GetBytes(original), key, iv);
            var decrypted = DecryptTripleDes(encrypted, key, iv);
            */

            // Triple DES
            var key = GenerateRandomNumber(32);
            var iv = GenerateRandomNumber(16);
            var encrypted = EncryptAes(Encoding.UTF8.GetBytes(original), key, iv);
            var decrypted = DecryptAes(encrypted, key, iv);
            var decryptedMessage = Encoding.UTF8.GetString(decrypted);

            return new
            {
                original = original,
                encrypted = Convert.ToBase64String(encrypted),
                decrypted = Convert.ToBase64String(decrypted),
                decryptedMessage = decryptedMessage
            };
        }


        #region DES
        internal byte[] Encrypt(byte[] tobeEcnrypt, byte[] key, byte[] iv)
        {
            using(var des = new DESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using(var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(), CryptoStreamMode.Write);
                    cryptoStream.Write(tobeEcnrypt, 0, tobeEcnrypt.Length);
                    cryptoStream.FlushFinalBlock();
                    return memoryStream.ToArray();
                }
            }
        }

        internal byte[] Decrypt(byte[] tobeEcnrypt, byte[] key, byte[] iv)
        {
            using (var des = new DESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(), CryptoStreamMode.Write);
                    cryptoStream.Write(tobeEcnrypt, 0, tobeEcnrypt.Length);
                    cryptoStream.FlushFinalBlock();
                    return memoryStream.ToArray();
                }
            }
        }
        #endregion

        #region AES
        internal byte[] EncryptAes(byte[] tobeEcnrypt, byte[] key, byte[] iv)
        {
            using (var des = new AesCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(), CryptoStreamMode.Write);
                    cryptoStream.Write(tobeEcnrypt, 0, tobeEcnrypt.Length);
                    cryptoStream.FlushFinalBlock();
                    return memoryStream.ToArray();
                }
            }
        }

        internal byte[] DecryptAes(byte[] tobeEcnrypt, byte[] key, byte[] iv)
        {
            using (var des = new AesCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(), CryptoStreamMode.Write);
                    cryptoStream.Write(tobeEcnrypt, 0, tobeEcnrypt.Length);
                    cryptoStream.FlushFinalBlock();
                    return memoryStream.ToArray();
                }
            }
        }
        #endregion

        #region TRIPLE DES
        internal byte[] EncryptTripleDes(byte[] tobeEcnrypt, byte[] key, byte[] iv)
        {
            using (var des = new TripleDESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(), CryptoStreamMode.Write);
                    cryptoStream.Write(tobeEcnrypt, 0, tobeEcnrypt.Length);
                    cryptoStream.FlushFinalBlock();
                    return memoryStream.ToArray();
                }
            }
        }

        internal byte[] DecryptTripleDes(byte[] tobeEcnrypt, byte[] key, byte[] iv)
        {
            using (var des = new TripleDESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(), CryptoStreamMode.Write);
                    cryptoStream.Write(tobeEcnrypt, 0, tobeEcnrypt.Length);
                    cryptoStream.FlushFinalBlock();
                    return memoryStream.ToArray();
                }
            }
        }
        #endregion

        #endregion


        #region Asymmentric Encryption


        internal object AsymmetricEncryption()
        {
            const string original = "Text to encrypt";

            // RSA with RSA parameters
            AsignNewKey();
            var encrypted = EncryptData(Encoding.UTF8.GetBytes(original));
            var decrypted = DecryptData(encrypted);
            

            // RSA with XML key
            /*var publicKeyPath = @"/Users/servin/Downloads/1_anayurt/publickey.xml";
            var privateKeyPath = @"/Users/servin/Downloads/1_anayurt/privatekey.xml";

            AsignNewKeyXML(privateKeyPath, publicKeyPath);

            var encrypted = EncryptDataXML(Encoding.UTF8.GetBytes(original), publicKeyPath);
            var decrypted = DecryptDataXML(encrypted, privateKeyPath);
            */

            // RSA with Csp key
            /*AssignKeyScp();

            var encrypted = EncryptDataCsp(Encoding.UTF8.GetBytes(original));
            var decrypted = DecryptDataCsp(encrypted);
            */
            var decryptedMessage = Encoding.UTF8.GetString(decrypted);

            return new
            {
                original = original,
                encrypted = Convert.ToBase64String(encrypted),
                decrypted = Convert.ToBase64String(decrypted),
                decryptedMessage = decryptedMessage
            };
        }

        #region RSA WITH RSA PARAMETERS KEY

        private RSAParameters _publicKey;
        private RSAParameters _privateKey;

        internal void AsignNewKey()
        {
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                _publicKey = rsa.ExportParameters(false);
                _privateKey = rsa.ExportParameters(true);
            }
        }

        public byte[] EncryptData(byte[] dataToEncrypt)
        {
            byte[] cipherbytes;
            using(var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                rsa.ImportParameters(_publicKey);
                cipherbytes = rsa.Encrypt(dataToEncrypt, true);
            }
            return cipherbytes;
        }

        public byte[] DecryptData(byte[] dataToEncrypt)
        {
            byte[] plain;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                rsa.ImportParameters(_privateKey);
                plain = rsa.Decrypt(dataToEncrypt, true);
            }
            return plain;
        }

        #endregion

        #region RSA with XAML keys

        internal void AsignNewKeyXML(string privateKeyPath, string publicKeyPath)
        {
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;

                if (File.Exists(privateKeyPath))
                {
                    File.Decrypt(privateKeyPath);
                }

                if (File.Exists(publicKeyPath))
                {
                    File.Decrypt(publicKeyPath);
                }

                var publicKeyFolder = Path.GetDirectoryName(publicKeyPath);
                var privateKeyFolder = Path.GetDirectoryName(privateKeyPath);

                if (!Directory.Exists(publicKeyFolder))
                {
                    Directory.CreateDirectory(publicKeyFolder);
                }
                if (!Directory.Exists(privateKeyFolder))
                {
                    Directory.CreateDirectory(privateKeyFolder);
                }

                File.WriteAllText(publicKeyPath, rsa.ToXmlString(false));
                File.WriteAllText(privateKeyPath, rsa.ToXmlString(true));
            }
        }


        public byte[] EncryptDataXML(byte[] dataToEncrypt, string publicKeyPath)
        {
            byte[] cipherbytes;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                rsa.FromXmlString(File.ReadAllText(publicKeyPath));
                cipherbytes = rsa.Encrypt(dataToEncrypt, false);
            }
            return cipherbytes;
        }

        public byte[] DecryptDataXML(byte[] dataToEncrypt, string privateKeyPath)
        {
            byte[] plain;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                rsa.FromXmlString(File.ReadAllText(privateKeyPath));
                plain = rsa.Decrypt(dataToEncrypt, false);
            }
            return plain;
        }

        #endregion

        #region RSA with CSP key

        const string ContainerName = "MyContainer";

        public void AssignKeyScp()
        {
            CspParameters cspParams = new CspParameters(1);
            cspParams.KeyContainerName = ContainerName;
            cspParams.Flags = CspProviderFlags.UseMachineKeyStore;
            cspParams.ProviderName = "Microsoft Strong Cryptographic Provider";
            var rsa = new RSACryptoServiceProvider(cspParams)
            {
                PersistKeyInCsp = true
            };
        }

        public void DeleteKeyInScp()
        {
            CspParameters cspParams = new CspParameters();
            cspParams.KeyContainerName = ContainerName;

            var rsa = new RSACryptoServiceProvider(cspParams)
            {
                PersistKeyInCsp = false
            };
            rsa.Clear();
        }


        public byte[] EncryptDataCsp(byte[] dataToEncrypt)
        {
            byte[] cipherbytes;

            CspParameters cspParams = new CspParameters { KeyContainerName = ContainerName };

            using (var rsa = new RSACryptoServiceProvider(2048, cspParams))
            {
                cipherbytes = rsa.Encrypt(dataToEncrypt, false);
            }
            return cipherbytes;
        }

        public byte[] DecryptDataCsp(byte[] dataToEncrypt)
        {
            byte[] plain;
            CspParameters cspParams = new CspParameters { KeyContainerName = ContainerName };

            using (var rsa = new RSACryptoServiceProvider(2048, cspParams))
            {
                plain = rsa.Decrypt(dataToEncrypt, false);
            }
            return plain;
        }
        #endregion

        #endregion


        #region Hybrid Encription

        internal object HybridEncryption()
        {
            const string original = "Text to encrypt";
            AsignNewKey();
            // Hybrid Encription
            /*
            var encryptedBlock = EncryptHybrid(Encoding.UTF8.GetBytes(original));
            var decrypted = DecryptHybrid(encryptedBlock);
            */

            // Hybrid Encription HMAC
            try
            {
                var encryptedBlock = EncryptHybridHmac(Encoding.UTF8.GetBytes(original));
                var decrypted = DecryptHybridHmac(encryptedBlock);

                return new
                {
                    original = original,
                    decrypted = Convert.ToBase64String(decrypted),
                    decryptedMessage = Encoding.UTF8.GetString(decrypted)
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    error = ex.Message
                };
            }
        }

        #region Hybrid Encription as is
        public EncryptedPacket EncryptHybrid(byte[] original)
        {
            var sessionKey = GenerateRandomNumber(32);

            var encryptedpacket = new EncryptedPacket { Iv = GenerateRandomNumber(16) };

            // encrypt the data with AES
            encryptedpacket.EncryptedData = EncryptAes(original, sessionKey, encryptedpacket.Iv);

            // encrypt the session key with RSA
            encryptedpacket.EncryptedSessionKey = EncryptData(sessionKey);

            return encryptedpacket;
        }

        public byte[] DecryptHybrid(EncryptedPacket encryptedPacket)
        {
            // Decrypt AES key with RSA
            var decryptedSessionKey = DecryptData(encryptedPacket.EncryptedSessionKey);

            // Decrypt our data with AES using the decrypted session key
            var decryptData = DecryptAes(encryptedPacket.EncryptedData, decryptedSessionKey, encryptedPacket.Iv);

            return decryptData;
        }
        #endregion


        #region Hybrid Encription HMAC
        public EncryptedPacket EncryptHybridHmac(byte[] original)
        {
            var sessionKey = GenerateRandomNumber(32);

            var encryptedpacket = new EncryptedPacket { Iv = GenerateRandomNumber(16) };

            // encrypt the data with AES
            encryptedpacket.EncryptedData = EncryptAes(original, sessionKey, encryptedpacket.Iv);

            // encrypt the session key with RSA
            encryptedpacket.EncryptedSessionKey = EncryptData(sessionKey);

            using(var hmac = new HMACSHA256(sessionKey))
            {
                encryptedpacket.Hmac = hmac.ComputeHash(encryptedpacket.EncryptedData);
            }

            return encryptedpacket;
        }

        public byte[] DecryptHybridHmac(EncryptedPacket encryptedPacket)
        {
            // Decrypt AES key with RSA
            var decryptedSessionKey = DecryptData(encryptedPacket.EncryptedSessionKey);

            using(var hmac = new HMACSHA256(decryptedSessionKey))
            {
                var hmacToCheck = hmac.ComputeHash(encryptedPacket.EncryptedData);

                if (!Compare(encryptedPacket.Hmac, hmacToCheck))
                {
                    throw new CryptographicException("hmac for decryption does not match");
                }
            }

            // Decrypt our data with AES using the decrypted session key
            var decryptData = DecryptAes(encryptedPacket.EncryptedData, decryptedSessionKey, encryptedPacket.Iv);

            return decryptData;
        }

        private bool Compare(byte[] array1, byte[] array2)
        {
            var result = array1.Length == array2.Length;

            for (int i = 0; i < array1.Length && i < array2.Length; i++)
            {
                result &= array1[i] == array2[i];
            }
            return result;
        }

        // Just Example. Do not use it for comparing byte arrays
        private bool CompareUnSecure(byte[] array1, byte[] array2)
        {
            if (array1.Length != array2.Length) return false;
            
            for (int i = 0; i < array1.Length; i++)
            {
                if (array1[i] != array2[i])
                    return false;
            }
            return true;
        }
        #endregion

        #endregion


        #region Digital Signature

        internal object DigitalSignature()
        {
            AsignNewKey();

            // Digital Signature
            /*
            var document = Encoding.UTF8.GetBytes("Text to encrypt");
            byte[] hashedDocument;

            using(var sha256 = SHA256.Create())
            {
                hashedDocument = sha256.ComputeHash(document);
            }
            var signature = SignData(hashedDocument);
            var verified = VerifySignature(hashedDocument, signature);
            

            return new
            {
                original = Encoding.Default.GetString(document),
                verified = verified
            };
            */

            //Hybrid HMAC encription with Digital Signature
            const string original = "Text to encrypt";
            var encryptedBlock = EncryptDigitalWithHybridHmac(Encoding.UTF8.GetBytes(original));
            var decrypted = DecryptDigitalWithHybridHmac(encryptedBlock);

            return new
            {
                original = original,
                decrypted = Convert.ToBase64String(decrypted),
                decryptedMessage = Encoding.UTF8.GetString(decrypted)
            };
        }

        private byte[] SignData(byte[] hashOfDataToSign)
        {
            using(var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.PersistKeyInCsp = false;
                rsa.ImportParameters(_privateKey);

                var rsaFormatter = new RSAPKCS1SignatureFormatter(rsa);
                rsaFormatter.SetHashAlgorithm("SHA256");

                return rsaFormatter.CreateSignature(hashOfDataToSign);
            }
        }

        private bool VerifySignature(byte[] hashOfDataToSign, byte[] signature)
        {
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                rsa.ImportParameters(_publicKey);

                var rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
                rsaDeformatter.SetHashAlgorithm("SHA256");

                return rsaDeformatter.VerifySignature(hashOfDataToSign, signature);
            }
        }

        #region Hybrid Encription HMAC with Digital Signature
        public EncryptedPacket EncryptDigitalWithHybridHmac(byte[] original)
        {
            var sessionKey = GenerateRandomNumber(32);

            var encryptedpacket = new EncryptedPacket { Iv = GenerateRandomNumber(16) };

            // encrypt the data with AES
            encryptedpacket.EncryptedData = EncryptAes(original, sessionKey, encryptedpacket.Iv);

            // encrypt the session key with RSA
            encryptedpacket.EncryptedSessionKey = EncryptData(sessionKey);

            using (var hmac = new HMACSHA256(sessionKey))
            {
                encryptedpacket.Hmac = hmac.ComputeHash(encryptedpacket.EncryptedData);
            }

            encryptedpacket.Signature = SignData(encryptedpacket.Hmac);

            return encryptedpacket;
        }

        public byte[] DecryptDigitalWithHybridHmac(EncryptedPacket encryptedPacket)
        {
            // Decrypt AES key with RSA
            var decryptedSessionKey = DecryptData(encryptedPacket.EncryptedSessionKey);

            using (var hmac = new HMACSHA256(decryptedSessionKey))
            {
                var hmacToCheck = hmac.ComputeHash(encryptedPacket.EncryptedData);

                if (!Compare(encryptedPacket.Hmac, hmacToCheck))
                {
                    throw new CryptographicException("hmac for decryption does not match");
                }

                if (!VerifySignature(encryptedPacket.Hmac, encryptedPacket.Signature))
                {
                    throw new CryptographicException("digital signature cannot be verified");
                }
            }

            // Decrypt our data with AES using the decrypted session key
            var decryptData = DecryptAes(encryptedPacket.EncryptedData, decryptedSessionKey, encryptedPacket.Iv);

            return decryptData;
        }
        #endregion

        #endregion


        #region Secure string

        internal object SecureStringTest()
        {
            /* min value in stack */
           List<int> buffer = new List<int> ();
            List<int> buffer2 = new List<int> ();
            Stack<int> input = new Stack<int>();
            input.Push(3);
            input.Push(4);
            input.Push(12);
            input.Push(65);
            input.Push(232);
            input.Push(2);
            /*
            foreach (var item in input)
            {
                buffer.Add(item);
            }*/

            while (true)
            {
                if (input.Count == 0)
                    break;
                int peek_res =input.Peek();
                if (peek_res == -1)
                    break;
                buffer.Add(peek_res);
                buffer2.Add(peek_res);
                input.Pop();
            }
            for(int i=buffer2.Count - 1; i >= 0; i--)
            {
                var item = buffer2[i];
                input.Push(item);
            }
            var res = (Int32)buffer.OrderBy(t => t).First();
            return new
            {
                res = res,
            };

            /*var lst = new List<string> { "Bob", "John", "Alice", "Ken" };

            var where = lst.Where(s => s == "Ken");
            var select = lst.Select(s => s == "Ken");
            var select2 = lst.Select(s => s);
            return new
            {
                where = where,
                select = select,
                select2 = select2
            };*/

            /*   var str = ToSecureString(new[] { '5', '1', '2', '5' });
               char[] charArray = CharacterData(str);
               string unsecure = ConvertToUnsecureString(str);


               return new
               {
                   secure = str,
                   charArray = charArray,
                   unsecure = unsecure
               };*/
        }

        private SecureString ToSecureString(char[] str)
        {
            var secureString = new SecureString();
            Array.ForEach(str, secureString.AppendChar);
            return secureString;
        }

        private char[] CharacterData(SecureString secureString)
        {
            char[] bytes;
            var ptr = IntPtr.Zero;

            try
            {
                ptr = Marshal.SecureStringToBSTR(secureString);
                bytes = new char[secureString.Length];

                Marshal.Copy(ptr, bytes, 0, secureString.Length);
            }
            finally
            {
                if (ptr != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(ptr);
                }
            }
            return bytes;
        }

        private string ConvertToUnsecureString(SecureString secureString)
        {
            if (secureString == null)
                throw new ArgumentException("secureString");

            var unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(secureString);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }
        #endregion
    }
}
