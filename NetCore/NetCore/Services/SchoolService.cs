﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NetCore.Models;
using NetCore.Models.Dto.Responses;
using System.Linq;
using ParrotWings.Core.Model;

namespace NetCore.Services
{
    public class SchoolService
    {
        private readonly LearnProjectContext _context;
        private readonly ILogger<SchoolService> _logger;

        public SchoolService(LearnProjectContext context, ILogger<SchoolService> logger)
        {
            _context = context;
            _logger = logger;
        }


        internal async Task<GetStudentsResponse> GetStudents()
        {
            try
            {
                var students = await _context.Students.ToListAsync();
                return new GetStudentsResponse()
                {
                    IsSuccess = true,
                    Students = students
                };
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, $"Getting user error: {ex.Message} {ex.StackTrace}");
                return new GetStudentsResponse()
                {
                    IsSuccess = false,
                    Students = null
                };
            }
        }

        internal async Task<PostTransactionResponse> PostTransaction(int studentId, string name)
        {
            try
            {
                var student = new Student
                {
                    Name = name
                };

                _context.Add(student);
                await _context.SaveChangesAsync();

                return new PostTransactionResponse()
                {
                    IsSuccess = true,
                    Student = student
                };
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, $"Deleting user error: {ex.Message} {ex.StackTrace}");
                return new PostTransactionResponse()
                {
                    IsSuccess = false,
                    Student = null
                };
            }
        }

        internal async Task<PostTransactionResponse> UpdateStudent(int studentId, Student student)
        {
            try
            {
                var targetStudents = await _context.Students
                    .Where(student => student.StudentId == studentId)
                    .ToListAsync();

                var targetStudent = targetStudents.FirstOrDefault();
                targetStudent.Name = student.Name;

                _context.Update(targetStudent);
                await _context.SaveChangesAsync();

                return new PostTransactionResponse()
                {
                    IsSuccess = true,
                    Student = student
                };
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, $"Deleting user error: {ex.Message} {ex.StackTrace}");
                return new PostTransactionResponse()
                {
                    IsSuccess = false,
                    Student = null
                };
            }
        }

        internal async Task<Response> DeleteStudent(int studentId)
        {
            try
            {
                var student = await _context.Students
                    .Where(student => student.StudentId == studentId)
                    .ToListAsync();

                _context.Remove<Student>(student.FirstOrDefault());
                await _context.SaveChangesAsync();

                return new Response()
                {
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, $"Deleting user error: {ex.Message} {ex.StackTrace}");
                return new Response()
                {
                    IsSuccess = false,
                };
            }
        }
        
    }
}
