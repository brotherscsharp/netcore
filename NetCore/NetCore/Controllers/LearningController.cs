﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetCore.Helpers;
using NetCore.Models;
using NetCore.Models.Dto.Responses;
using NetCore.Services;

namespace NetCore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LearningController : ControllerBase
    {
        private readonly LearningService _learningService;

        public LearningController(LearningService learningService)
        {
            _learningService = learningService;
        }

        [HttpGet]
        public async Task<ActionResult<GetStudentsResponse>> Get()
        {
            //var response = _learningService.GenerateRandomNumberAsList(10, 10);
            //var response = _learningService.Hashing();
            //var response = _learningService.SymmetricEncryption();
            //var response = _learningService.AsymmetricEncryption();
            //var response = _learningService.HybridEncryption();
            //var response = _learningService.DigitalSignature();
            var response = _learningService.SecureStringTest();
            

            return Ok(response);
        }
    }
}
