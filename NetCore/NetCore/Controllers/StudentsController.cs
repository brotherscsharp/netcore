﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetCore.Models;
using NetCore.Models.Dto.Responses;
using NetCore.Services;
using ParrotWings.Core.Model;

namespace NetCore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly SchoolService _schoolService;

        public StudentsController(SchoolService schoolService)
        {
            _schoolService = schoolService;
        }

        // GET: api/Students
        [HttpGet]
        public async Task<ActionResult<GetStudentsResponse>> GetStudents(string filter = null)
        {
            var response = await _schoolService.GetStudents();
            if (!response.IsSuccess)
                BadRequest(response);
            return Ok(response);
        }

        // POST: api/Students
        [HttpPost]
        public async Task<ActionResult<PostTransactionResponse>> PostTransaction(Student student)
        {
            var response = await _schoolService.PostTransaction(student.StudentId, student.Name);
            if (!response.IsSuccess)
                return BadRequest(response);
            return Ok(response);
        }

        // PUT: api/Students/ID
        [HttpPut("{id:int}")]
        public async Task<ActionResult<PostTransactionResponse>> UpdateEmployee(int id, Student student)
        {
            var response = await _schoolService.UpdateStudent(id, student);
            if (!response.IsSuccess)
                return BadRequest(response);
           
            return Ok(response);            
        }

        // DELETE: api/Students/ID
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<GetStudentsResponse>> DeleteStudents(int id)
        {
            var response = await _schoolService.DeleteStudent(id);
            if (!response.IsSuccess)
                BadRequest(response);
            return Ok(response);
        }
    }
}
