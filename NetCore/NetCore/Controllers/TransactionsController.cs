﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ParrotWings.Backend.Services.Services;
using ParrotWings.Core.Model;
using ParrotWings.Core.Model.Dto.Requests;
using ParrotWings.Core.Model.Dto.Value;

namespace NetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly TransactionsService _transactionsService;
        private IHttpContextAccessor _httpContextAccessor;

        public TransactionsController(TransactionsService transactionsService, IHttpContextAccessor httpContextAccessor)
        {
            _transactionsService = transactionsService;
            _httpContextAccessor = httpContextAccessor;
        }

        // user's transactions
        // GET: api/Transactions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TransactionHistory>>> GetTransactions(long userId)
        {
            var response = await _transactionsService.GetUsersTransactions(userId);
            if (!response.IsSuccess)
                return BadRequest(response);
            return Ok(response);
        }

        // POST: api/Transactions
        [HttpPost]
        public async Task<ActionResult<Transaction>> PostTransaction(CreateTransactionRequest transaction)
        {
            // long.TryParse(HttpContext?.User?.FindFirst("id")?.Value, out long userId); // does not work from browser. with postman works correct
            var userId = transaction.SenderUserId;
            var response = await _transactionsService.CreateTransaction(transaction.UserId, transaction.Amount, userId);
            if (!response.IsSuccess)
                return BadRequest(response);
            return Ok(response);
        }
    }
}
