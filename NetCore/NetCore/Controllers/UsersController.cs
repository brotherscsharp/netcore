﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParrotWings.Backend.Services.Services;
using ParrotWings.Core.Model;
using ParrotWings.Core.Model.Dto.Requests;
using ParrotWings.Core.Model.Dto.Responses;

namespace NetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly AccountService _accountService;

        public UsersController(AccountService accountService)
        {
            _accountService = accountService;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<GetUsersResponse>> GetUsers(string filter = null)
        {
            var response = await _accountService.GetUsers(filter);
            if (!response.IsSuccess)
                BadRequest(response);
            return Ok(response);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(long id)
        {
            var response = await _accountService.GetUser(id);
            if (response == null)
            {
                BadRequest("User not found");
            }
            return Ok(response);
        }

        // POST: /auth/register
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Route("/auth/register")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<SignUpResponse>> SignUp(User user)
        {
            var response = await _accountService.SignUp(user);
            if (!response.IsSuccess)
                BadRequest(response);
            return Ok(response);
        }

        // POST: api/Users
        [Route("/auth/login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<SignUpResponse>> SignIn([FromBody] SignInRequest request)
        {
            var response = await _accountService.SignIn(request);
            if (!response.IsSuccess)
                BadRequest(response);
            return Ok(response);
        }
    }
}
