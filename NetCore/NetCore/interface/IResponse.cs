﻿namespace NetCore.@interface
{
    public interface IResponse
    {
        bool IsSuccess { get; set; }
        string Error { get; set; }
    }
}
