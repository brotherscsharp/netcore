﻿using System;
using ParrotWings.Core.Model;

namespace NetCore.Models.Dto.Responses
{
    public class PostTransactionResponse : Response
    {
        public Student Student { get; set; }
    }
}
