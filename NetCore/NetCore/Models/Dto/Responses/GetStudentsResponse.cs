﻿using System.Collections.Generic;
using ParrotWings.Core.Model;

namespace NetCore.Models.Dto.Responses
{
    public class GetStudentsResponse: Response
    {
        public IEnumerable<Student> Students { get; set; }
    }
}
