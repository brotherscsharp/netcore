﻿using System;
using NetCore.@interface;

namespace NetCore.Models.Dto.Responses
{
    public class Response : IResponse
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
    }
}
